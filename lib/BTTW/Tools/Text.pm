package BTTW::Tools::Text;
our $VERSION = '0.012';
use Moose;

use Moose::Util::TypeConstraints qw[union duck_type];

use BTTW::Tools;

use Lingua::Identify qw[langof];
use Lingua::Stem::Snowball qw[stem];
use Lingua::StopWords qw[getStopWords];

=head1 NAME

BTTW::Tools::Text - (Rich)text utilities

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 source

This required attribute holds a reference or scalar resource that will be
used as a source of plaintext data.

If passed an object, the object must implement the C<plaintext> method, which
is expected to return a unicode perl string.

=cut

has source => (
    is => 'ro',
    required => 1,
    isa => union([
        'Str',
        duck_type([ 'plaintext' ])
    ])
);

=head2 encoding

This attribute holds a string representation of the encoding scheme.

=cut

has encoding => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    default => 'UTF-8'
);

=head2 language

This attribute holds a string representation of the language of the supplied
plaintext source. If not supplied, this class will attempt to use
L<Lingua::Identify> to get the most probable language.

=head3 Interface

=over 4

=item has_language

Value predicate for this attribute.

=item build_language

Our implementation of this attribute's value.

=back

=cut

has language => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    predicate => 'has_language',
    builder => 'build_language'
);

=head2 plaintext

This attribute holds a memoized version of the plaintext representation of the
text source. It can be provided up front, or we will coax the data from the
L</source> attribute.

=head3 Interface

=over 4

=item has_plaintext

Value predicate for this attribute.

=item build_plaintext

Our implementation of this attribute's value.

=back

=cut

has plaintext => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    predicate => 'has_plaintext',
    builder => 'build_plaintext'
);

=head2 wordlist

This lazy attribute holds an array of strings that represent the wordlist for
the provided source. Duplicate words will be stripped.

=head3 Interface

=over 4

=item has_wordlist

Value predicate for this attribute.

=item build_wordlist

Our implementation for this attribute's value.

=item words

Return all items in the array as a list.

=item map_words

Map over all the items in the array.

=back

=cut

has wordlist => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    lazy => 1,
    predicate => 'has_wordlist',
    builder => 'build_wordlist',
    handles => {
        words => 'elements',
        map_words => 'map'
    }
);

=head2 normalized_wordlist

This lazy method holds an array of strings that represent the normalized
wordlist for the provided source. If not explicitly set or provided during
construction it will be derived from L</wordlist>, filtered through
L<Lingua::StopWords/getStopWords>.

=head3 Interface

=over 4

=item has_normalized_wordlist

Value predicate for this attribute.

=item build_normalized_wordlist

Our implementation for this attribute's value.

=item normalized_words

Return all items in the array as a list.

=item map_normalized_words

Map over all the items in the array.

=back

=cut

has normalized_wordlist => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    lazy => 1,
    predicate => 'has_normalized_wordlist',
    builder => 'build_normalized_wordlist',
    handles => {
        normalized_words => 'elements',
        map_normalized_words => 'map'
    }
);

=head2 stemmed_wordlist

This lazy attribute holds an array of strings that represent the normalized
and stemmed wordlist for the provided source. If not explicitly set or
provided during construction, it will be derived from L</normalized_wordlist>
by applying L<Lingua::Stem::Snowball> on the normalized words.

=head3 Interface

=over 4

=item has_stemmed_wordlist

Value predicate for this attribute.

=item build_stemmed_wordlist

Our implementation for this attribute's value.

=item stemmed_words

Return all items in the array as a list.

=item map_stemmed_words

Map over all items in the array.

=back

=cut

has stemmed_wordlist => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    lazy => 1,
    predicate => 'has_stemmed_wordlist',
    builder => 'build_stemmed_wordlist',
    handles => {
        stemmed_words => 'elements',
        map_stemmed_words => 'map'
    }
);

=head1 BUILDERS

=head2 build_language

Default value builder implementation for the L</language> attribute.

=cut

sub build_language {
    my $self = shift;

    # The language of very short strings can't be identified reliably, but we
    # still want to index the text from those files. Assume Dutch in those
    # cases.
    return langof($self->plaintext) // 'nl';
}

=head2 build_plaintext

Default value builder implementation for the L</plaintext> attribute.

=cut

sub build_plaintext {
    my $self = shift;

    return blessed $self->source ? $self->source->plaintext : $self->source;
}

=head2 build_wordlist

Default value builder implementation for the L</wordlist> attribute.

=cut

sub build_wordlist {
    my $self = shift;

    my %wordlist = map { lc($_) => undef } ($self->plaintext =~ m[(\w+)]g);

    return [ keys %wordlist ];
}

=head2 build_normalized_wordlist

Default value builder implementation for the L</normalized_wordlist>
attribute.

=cut

sub build_normalized_wordlist {
    my $self = shift;

    my $stopwords = getStopWords($self->language, $self->encoding);

    return [
        grep { not exists $stopwords->{ $_ } } $self->words
    ];
}

=head2 build_stemmed_wordlist

Default value builder implementation for the L</stemmed_wordlist> attribute.

=cut

sub build_stemmed_wordlist {
    my $self = shift;

    my %wordlist = map {
        $_ => undef
    } stem($self->language, $self->normalized_wordlist);

    return [ keys %wordlist ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
