package BTTW::Tools::MergeObject;
our $VERSION = '0.012';
use warnings;
use strict;

use autodie;
use Try::Tiny;
use BTTW::Tools;
use v5.14;
use Scalar::Util qw(reftype);
use Data::Dumper;

use Exporter qw(import);

our @EXPORT_OK = qw(get_diff merge_dbix_row merge_moose_obj);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

=head1 NAME

BTTW::Tools::MergeObjects - Merge moose objects with data and/or DB rows

=head1 DESCRIPTION

=head1 SYNOPSIS

=head2 get_diff

Get a diff from objects and/or database objects to see what needs to be merged.

=cut

sub get_diff {
    my ($object, $hash) = @_;

    # DBIX::Resulset
    my @columns;
    if ($object->can('get_columns')) {
        @columns = @{$object->get_columns};
    }
    # moose
    elsif($object->can('meta') && $object->meta->can('get_all_attributes')) {
        @columns = map { $_->name } $object->meta->get_all_attributes;
    }

    my %diff;
    my ($orig, $new);

    foreach my $attr (@columns) {

        next if !exists $hash->{$attr};

        $orig = $object->$attr;
        $new  = $hash->{$attr};

        if (($orig// '') ne ($new // '')) {
            $diff{$attr} = $new;
        }

    }
    return \%diff;
}

=head2 merge_dbix_row

Merge DBIx::Class row objects with a hash containing values

=cut

sub merge_dbix_row {
    my ($object, $hash) = @_;

    my $diff = get_diff($object, $hash);
    $object->update($diff);
    return $object;
}

=head2 merge_moose_obj

Merge Moose objects with a hash containing values

=cut


sub merge_moose_obj {
    my ($object, $hash) = @_;

    my $diff = get_diff($object, $hash);
    foreach (keys %$diff) {
        if (!defined $diff->{$_} ) {
            my $attr = $object->meta->find_attribute_by_name($_);
            if ($attr->has_clearer) {
                $attr->clearer;
                next;
            }
        }
        else {
            $object->$_($diff->{$_});
        }
    }
    return $object;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
