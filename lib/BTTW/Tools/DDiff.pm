package BTTW::Tools::DDiff;
our $VERSION = '0.012';
use warnings;
use strict;

use Exporter 'import';

our @EXPORT = qw[ddiff ddiff_stringify];

=head1 NAME

BTTW::Tools::DDiff - Pretty-print changes in plain perl datastructures
in dutch.

=head1 FUNCTIONS

=head2 ddiff

Takes two values and produces a list of differences.

    my ($change) = ddiff({ a => 1 }, { a => 2 });

Produces

    # $change
    {
        op     => 'update_hash_value',
        key    => 'a',
        update => {
            op => 'update',
            from => 1,
            to => 2
        }
    }

=cut

sub ddiff {
    my ($a, $ref_a, $b, $ref_b) = map { $_, ref } @_;

    # Simplest no-op, was null, still null.
    return unless defined $a || defined $b;

    if (defined $a xor defined $b) {
        return defined $a
               ? { op => 'unset', from => $a }
               : { op => 'set', to => $b };
    }

    if ($ref_a ne $ref_b) {
        return { op => 'update', from => $a, to => $b };
    }

    if ($ref_a eq '') {
        return { op => 'update', from => $a, to => $b } if $a ne $b;
        return;
    }

    if ($ref_a eq 'ARRAY') {
        my @changes;

        # In-place updates
        for my $index (0 .. ($#$a > $#$b ? $#$a : $#$b)) {
            push @changes, {
                op => 'update_array_element',
                index => $index,
                update => $_
            } for ddiff($a->[$index], $b->[$index]);
        }

        return @changes;
    }

    if ($ref_a eq 'HASH') {
        my @changes;

        # Make a set of all keys in $a and $b
        my %keys = map { $_ => 1 } (keys %{ $a }, keys %{ $b });

        # Check ddiff for every key
        for my $key (sort keys %keys) {
            push @changes, {
                op => 'update_hash_value',
                key => $key,
                update => $_
            } for ddiff($a->{ $key }, $b->{ $key });
        }

        return @changes;
    }
}

=head2 ddiff_stringify

Takes a list of changes produced by L</ddiff>, and returns a list of
pretty-printed strings (in Dutch).

=cut

sub ddiff_stringify {
    my @lines;

    for my $change (@_) {
        if ($change->{ op } eq 'set') {
            push @lines, sprintf(
                'nieuwe waarde: %s',
                _ddiff_stringify_value($change->{ to })
            );

            next;
        }

        if ($change->{ op } eq 'unset') {
            push @lines, sprintf(
                'waarde verwijderd: %s',
                _ddiff_stringify_value($change->{ from })
            );

            next;
        }

        if ($change->{ op } eq 'update') {
            push @lines, sprintf(
                'waarde gewijzigd van %s naar %s',
                _ddiff_stringify_value($change->{ from }),
                _ddiff_stringify_value($change->{ to }),
            );

            next;
        }

        if ($change->{ op } eq 'update_array_element') {
            push @lines, sprintf(
                'element %d: %s',
                $change->{ index },
                ddiff_stringify($change->{ update })
            );

            next;
        }

        if ($change->{ op } eq 'update_hash_value') {
            push @lines, sprintf(
                'veld "%s": %s',
                $change->{ key },
                ddiff_stringify($change->{ update })
            );

            next;
        }
    }

    return @lines;
}

sub _ddiff_stringify_value {
    my ($value, $ref) = map { $_, ref } @_;

    return sprintf('"%s"', $value || '') unless $ref;

    if ($ref eq 'HASH') {
        return 'velden';
        return sprintf('velden: (%s)', join(', ', map {
            sprintf('"%s": %s', $_, _ddiff_stringify_value($value->{ $_ }))
        } keys %{ $value }));
    }

    if ($ref eq 'ARRAY') {
        return 'lijst';
        return sprintf('lijst: (%s)', join(', ', map {
            _ddiff_stringify_value($value->[ $_ ])
        } 0 .. $#$value));
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
