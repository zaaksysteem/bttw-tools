package BTTW::Tools::Text::HTML;
our $VERSION = '0.012';
use Moose;

use BTTW::Tools;

use HTML::Parser;

=head1 NAME

BTTW::Tools::Text::HTML - Provide a plaintext interface to HTML files

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 file

This attribute should hold the path to an HTML file to be converted.

=cut

has file => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 plaintext

=head3 Interface

=over 4

=item has_plaintext

Value predicate for this attribute.

=item build_plaintext

Our implementation of this attribute's value.

=back

=cut

has plaintext => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_plaintext',
    builder => 'build_plaintext'
);

=head1 BUILDERS

=head2 build_plaintext

Default value builder implementation for the L</plaintext> attribute.

=cut

sub build_plaintext {
    my $self = shift;
    my $text = '';

    my $parser = HTML::Parser->new(
        api_version => 3,
        text_h => [ sub { $text .= shift }, 'dtext' ]
    );

    $parser->parse_file($self->file);

    return $text;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
