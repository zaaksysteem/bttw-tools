package BTTW::Types;
our $VERSION = '0.012';
use base 'MooseX::Types::Combine';

=head1 NAME

BTTW::Types - Collected Moose TypeConstraints

=head1 DESCRIPTION

This package combines several type libraries into a single namespace for ease
of use.

=head1 TYPE LIBRARIES

=head2 L<BTTW::Types::Core>

=cut

__PACKAGE__->provide_types_from(qw[
    BTTW::Types::Core
]);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
