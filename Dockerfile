FROM registry.gitlab.com/opndev/perl5/docker-p5/moosy:latest

RUN apt-get update && apt-get install --no-install-recommends -y \
    # Required by File::ArchivableFormat' File::LibMagic dependency
    libmagic-dev \
    # Required for pdftotext
    poppler-utils \
 && rm -rf /var/lib/apt/lists/*

# Install dependency
RUN docker-cpanm https://gitlab.com/waterkip/file-archivableformats.git@v1.5.3
RUN docker-cpanm File::ShareDir::Install
COPY cpanfile .
RUN docker-cpanm --installdeps .
COPY . .
RUN docker-cpanm . && make clean
